PiDP Networking
===============

This project contains my personal notes related to a PiDP-10 running the
ITS operating system.  I'm making my notes available in the hope that
others will find them useful.

Links to other sites
====================

You can read more about the PiDP-10 here:

https://obsolescence.wixsite.com/obsolescence/pidp10

For more information about ITS, visit:

https://github.com/PDP-10/its

For more information about Chaosnets, visit:

https://github.com/bictorv/chaosnet-bridge

https://chaosnet.net/

Ethernet Bridging
=================

I want a static IP address to the ITS system running on my PiDP-10.  That means
ITS and the Raspberry Pi of the PiDP-10 must share an Ethernet connection.  We
can use a bridged TAP interface.  Here is how to set that up.

imp.odt - How to set up the Interface Message Processor (IMP) - this file can be opened on your Pi via "libreoffice imp.odt".

imp.pdf - the same document, but as a pdf file, for convenience.

pdp10-bridging - this is the config file that I am using on my system.  You can use it as a template, but you have to change my user name (sfalco) to whatever user name you are using on your Pi, and you will want to change the MAC address to the one belonging to your Pi.  This is described more fully in the imp.pdf file, so please start there.

Chaosnet Bridging
=================

While the original PDP-10 systems at MIT did have Ethernet, they also made
heavy use of a home-grown LAN called "chaosnet".  I want to be able to access
ITS over chaosnet from various machines in my house, so I need to set up a
local chaosnet.  Here is how to do that.

chaosnet.odt - How to extend chaosnet to additional Linux systems.  I mainly
use this so I can run mlftp to move files between my desktop machine and my
PiDP-10.

chaosnet.pdf - the same document, but as a pdf file, for convenience.

desktop.cbridge.conf - this is the cbridge configuration file for my desktop
machine.

rpi.cbridge.conf = this is the cbridge configuration file to the Raspberry Pi.

chaos-hosts - my chaosnet participants.
